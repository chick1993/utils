## 写入文件 util\sheet\Write

### 1.支持的文件类型

- [x] xlsx
- [ ] csv

### 2.调用示例

```php
// 以下涉及到的变量
$header = ['a0','b0'];
$data = [['a1','b1'],['a2','b2']];
$filename = 'xxx/xxx.xlsx';
```

- 1.保存数据到文件

```php
Write::file('xxx/xxx.xlsx',‘sheet1‘)->header($header)->data($data)->save();
```

- 2.导出数据到浏览器

```php
Write::file($filename)->header($header)->data($data)->output();
```

- 3.下载导入时产生的异常数据

```php
Write::file($filename)->errors($read)->output();
````

## 读取文件 util\sheet\Read

### 1.支持的文件类型

- [x] xlsx
- [ ] csv

### 2.调用示例

```php
// 以下涉及到的变量
$filename = 'xxx/xxx.xlsx';
// 列索引=>字段名
$keys = [0=>'filed0',1=>'field1',3=>'field3',...];
```

- 1。读取数据

```php
$read = Read::file($filename)->relation($keys);
$data = $read->getArray(1);
 ```

- 2.读取、验证并导出错误数据

```php
$read = Read::file($filename)->relation($keys);
$header = $read->getRow(0);
$data = $read->valid(function ($row, $num) {
  // 直接抛出单个异常
  // if (empty($row['xxx'])) {
  //    throw new ColException(1, '不能为空');
  // }
  // 抛出多个异常
    $exception = [];
    if (empty($row['xxx1'])) {
        $exception[] = new ColException(1, '不能为空');
    }
    if (empty($row['xxx2'])) {
        $exception[] = new ColException(2, '不能为空');
    }
    throw new ColsException($exception);
})->getArray(1);
if ($read->hasRowException()) {
  Write::file(runtime_path() . 'export/test.xlsx')
    ->errors($read)->output('导出错误');
}
```

  

