<?php

namespace chick1993\util\libs\exceptions;

class FileNotFoundException extends RuntimeException
{
    /**
     * @param string $message 异常信息
     * @param int $code
     */
    public function __construct(string $message = '文件未找到', int $code = 400)
    {
        parent::__construct($message, $code);
    }


}