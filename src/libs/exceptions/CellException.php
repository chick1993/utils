<?php

namespace chick1993\util\libs\exceptions;

class CellException extends RuntimeException
{
    /**
     * @param int $col 异常列 0-第一列
     * @param int $row 异常行 0-第一行
     * @param string $message 异常信息
     * @param int $bgColor 填充色，16进制整数
     * @param int $color 文字色，16进制整数
     */
    public function __construct(int $col, int $row = 0, string $message = '数据不合规，请检查', int $bgColor = 0xffc000, int $color = 0x00)
    {
        parent::__construct($message);
        $this->data = [
            'msg'     => $message,
            'col'     => $col,
            'row'     => $row,
            'bgColor' => $bgColor,
            'color'   => $color,
        ];
    }

    /**
     * @param int $col 0-第一列
     * @return self
     */
    public function setColIndex(int $col): self
    {
        $this->data['col'] = $col;
        return $this;
    }

    /**
     * @param int $row 0-第一行
     * @return self
     */
    public function setRowIndex(int $row): self
    {
        $this->data['row'] = $row;
        return $this;
    }
}