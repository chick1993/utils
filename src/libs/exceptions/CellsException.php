<?php

namespace chick1993\util\libs\exceptions;

class CellsException extends RuntimeException
{
    /**
     * @param []CellException $exceptions 异常列数据 0-第一列
     */
    public function __construct($exceptions = [], string $message = '数据不合规，请检查')
    {
        parent::__construct($message);
        foreach ($exceptions as $d) {
            if ($d instanceof CellException) $d = $d->getError();
            $this->data[] = [
                'msg'     => $d['msg'] ?? $message,
                'col'     => $d['col'] ?? 0,
                'row'     => $d['row'] ?? 0,
                'color'   => $d['color'] ?? 0x00,
                'bgColor' => $d['bgColor'] ?? 0xffc000,
            ];
        }

    }

    /**
     * 添加异常
     * @param CellException $e
     * @return self
     */
    public function add(CellException $e): self
    {
        $this->data[] = $e->getError();
        return $this;
    }

    /**
     * @param int $col 0-第一列
     * @return void
     */
    public function setColIndex(int $col)
    {
        foreach ($this->data as $k => $d) {
            $this->data['col'] = $col;
        }
    }

    /**
     * @param int $row 0-第一行
     * @return self
     */
    public function setRowIndex(int $row): self
    {
        foreach ($this->data as $k => $d) {
            $this->data[$k]['row'] = $row;
        }
        return $this;
    }
}